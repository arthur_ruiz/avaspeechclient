﻿using Microsoft.CognitiveServices.SpeechRecognition;
using System;

namespace AvaSpeechClient
{
    public class SpeechToTextHandler
    {

        private bool _started;
        private string _key;
        private TextToSpeechStatus _status;
        private MicrophoneRecognitionClient _micClient;

        public EventHandler<SpeechToTextResponseEventArgs> OnTextOutput = null;
        public EventHandler<SpeechToTextStatusEventArgs> OnStatusChange = null;
        

        public SpeechToTextHandler(string key)
        {
            _key = key;
            _started = false;
        }

        public void Start()
        {
            if (!_started)
            {
                _started = true;
                SetupMicrophone();
                Listen();
            }
        }

        public void Stop()
        {
            _started = false;
        }

        private void SetupMicrophone()
        {
            _micClient = SpeechRecognitionServiceFactory.CreateMicrophoneClient(
                SpeechRecognitionMode.ShortPhrase, 
                "en-US", 
                this._key
            );

            _micClient.OnMicrophoneStatus += _micClient_OnMicrophoneStatus;
            _micClient.OnResponseReceived += _micClient_OnResponseReceived;
            _micClient.OnConversationError += _micClient_OnConversationError;
        }

        private void Listen()
        {
            if (_started)
            {
                _micClient.StartMicAndRecognition();

            }
        }

        private void _micClient_OnMicrophoneStatus(object sender, MicrophoneEventArgs e)
        {
            if(e.Recording)
            {
                ChangeStatus(TextToSpeechStatus.Listening);
            }
        }

        private void _micClient_OnResponseReceived(object sender, SpeechResponseEventArgs e)
        {
            ChangeStatus(TextToSpeechStatus.Processing);

            _micClient.EndMicAndRecognition();

            if(OnTextOutput != null && e.PhraseResponse.Results.Length > 0)
            {
                var responseText = e.PhraseResponse.Results[0].DisplayText;
                OnTextOutput(this, new SpeechToTextResponseEventArgs()
                {
                    Response = responseText
                });
            } else
            {
                Console.WriteLine("No Handler specified");
            }

            Listen();
        }

        private void _micClient_OnConversationError(object sender, SpeechErrorEventArgs e)
        {
            Console.WriteLine("An error occurred while processing speech.");
        }

        private void ChangeStatus(TextToSpeechStatus status)
        {
            _status = status;
            if(OnStatusChange != null)
            {
                OnStatusChange(this, new SpeechToTextStatusEventArgs()
                {
                    Status = status
                });
            }
        }
    }

    public class SpeechToTextResponseEventArgs: EventArgs
    {
        public string Response { get; set; }
    }

    public class SpeechToTextStatusEventArgs : EventArgs
    {
        public TextToSpeechStatus Status { get; set; }
    }

    public enum TextToSpeechStatus
    {
        Listening,
        Processing
    }
}
