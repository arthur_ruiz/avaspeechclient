﻿using System;
using System.IO;
using System.Media;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace AvaSpeechClient
{
    public class TextToSpeechHandler
    {

        private string _key;
        private string _lastToken;
        private DateTime? _lastTokenGeneration;

        private string _jwtUrl = "https://api.cognitive.microsoft.com/sts/v1.0/issueToken";
        private string _synthesizeUrl = "https://speech.platform.bing.com/synthesize";

        

        public TextToSpeechHandler(string key)
        {
            _key = key;
            _lastToken = string.Empty;
            _lastTokenGeneration = null;
        }
        public void Convert(string text)
        {
            //Get token key
            var token = "";
            if (isTokenExpired())
            {
                var tokenTask = GenerateToken();
                tokenTask.Wait();
                token = tokenTask.Result;

                _lastToken = token;
                _lastTokenGeneration = DateTime.Now;
            } else
            {
                token = _lastToken;
            }

            //Synthesize
            var audioTask = Synthesize(token, text);
            audioTask.Wait();
            var audio = audioTask.Result;

            //Play Audio
            Play(audio);
        }

        private async Task<string> GenerateToken()
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", this._key);
            var response = await client.PostAsync(this._jwtUrl, null);
            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsStringAsync();
        }

        private async Task<byte[]> Synthesize(string token, string text)
        {
            var ssmlXML = @"<speak version='1.0' xml:lang='en-US'>
                                <voice xml:lang='en-US' xml:gender='Female' 
                                       name='Microsoft Server Speech Text to Speech Voice (en-US, ZiraRUS)'>
                                    {0}
                                </voice>
                            </speak>";

            var ssmlContent = new StringContent(String.Format(ssmlXML, text), Encoding.UTF8, "application/xml");

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            client.DefaultRequestHeaders.Add("X-Microsoft-OutputFormat", "riff-16khz-16bit-mono-pcm");
            client.DefaultRequestHeaders.Add("User-Agent", "Ava");
            var response = await client.PostAsync(this._synthesizeUrl, ssmlContent);
            response.EnsureSuccessStatusCode();

            var result = await response.Content.ReadAsByteArrayAsync();
            return result;
        }

        private void Play(byte[] audio)
        {
            using(MemoryStream stream = new MemoryStream(audio))
            {
                using (SoundPlayer player = new SoundPlayer(stream))
                {
                    player.Play();
                }
            }
        }

        private bool isTokenExpired()
        {
            if(_lastToken == string.Empty || !_lastTokenGeneration.HasValue)
            {
                return true;
            }

            var minutes = DateTime.Now.Subtract(_lastTokenGeneration.Value).TotalMinutes;
            if(minutes > 8)
            {
                return true;
            }

            return false;
        }
    }
}
