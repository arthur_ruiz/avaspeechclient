﻿using System;

namespace AvaSpeechClientSample
{
    class Program
    {
        static void Main(string[] args)
        {
            SpeechAPIDemo demo = new SpeechAPIDemo();
            demo.Start();

            Console.Read();
            demo.Stop();

            Console.WriteLine("Stopped");
        }
    }
}
