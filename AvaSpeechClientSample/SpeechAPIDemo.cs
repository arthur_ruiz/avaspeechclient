﻿using AvaSpeechClient;
using System;

namespace AvaSpeechClientSample
{
    public class SpeechAPIDemo
    {

        private SpeechToTextHandler _speechToTextHandler;
        private TextToSpeechHandler _textToSpeechHandler;

        public SpeechAPIDemo()
        {
            _speechToTextHandler = new SpeechToTextHandler("129d330d8b174fd1839a422315804fa0");
            _textToSpeechHandler = new TextToSpeechHandler("129d330d8b174fd1839a422315804fa0");
        }
        public void Start()
        {
            _speechToTextHandler.OnTextOutput += OnSpeechToTextOutput;
            _speechToTextHandler.OnStatusChange += OnSpeectToTextStatusChange;
            _speechToTextHandler.Start();
        }

        public void Stop()
        {
            if(_speechToTextHandler != null)
            {
                _speechToTextHandler.Stop();
            }
        }

        private void OnSpeechToTextOutput(Object sender, SpeechToTextResponseEventArgs e)
        {
            if(!string.IsNullOrEmpty(e.Response))
            {
                var text = e.Response;
                Console.WriteLine(text);
                _textToSpeechHandler.Convert(text);
            } else
            {
                Console.WriteLine("No Response");
            }
        }

        private void OnSpeectToTextStatusChange(Object sender, SpeechToTextStatusEventArgs e)
        {
            Console.WriteLine(e.Status.ToString());
        }
    }
}
