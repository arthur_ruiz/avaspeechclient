# AvaSpeechClient

A C# client(wrapper) library that converts ``speech to text`` and ``text to speech``. It uses the Microsoft Cognitive Services. 

## Source Structure

The solution contains 2 projects:
1. ``AvaSpeechClient`` - A library project that wraps Microsoft Cognitive Services to convert speech to text and vice versa.
2. ``AvaSpeechClientSample`` - A sample console application that uses the DLL from AvaSpeechClient that listens to your speech via microphone and repeats what you have said. It also displays the text in the console.

## Library

#### SpeechToTextHandler

Use this object to convert Speech to Text.

1. ```Constructor(string key)``` - Pass in the subscription key from Microsoft Cognitive Service.
2. ```Start()``` - Call this to start the listening process. This method will continue to listen to incoming speech via microphone until you call ```Stop()```
3. ```Stop()``` -  This function stop the listening process.
4. ```OnTextOutput``` - This event is fired after a speech is converted to text. Event Args will contain the text value. This is where you would like to put text processing and call the ```TextToSpeechHandler's Convert()``` to convert the text to speech.
5. ```OnStatusChange``` -  This event is fired when the handler is either listening or processing.

#### TextToSpeechHandler

Use this object to convert Text to Speech.

1. ```Constructor(string key)``` - Pass in the subscription key from Microsoft Cognitive Service.
2. ```Convert(string text)``` - Call this to convert the provided text in to speech. It will automatically play an audio version of the text provided in the parameter.
